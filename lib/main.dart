import 'package:check_box/checkBoxTilewidget.dart';
import 'package:check_box/checkbox_widget.dart';
import 'package:check_box/dropdown_widget.dart';
import 'package:check_box/radio_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'UI Extension',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Ui Extension')),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('Ui Menu'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('CheckBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxwidget()));
              },
            ),
            ListTile(
              title: Text('CheckBoxTile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTilewidget()));
              },
            ),
            ListTile(
              title: Text('DropDown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownwidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Radiowiget()));
              },
            )
          ],
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxwidget()));
            },
          ),
          ListTile(
            title: Text('CheckBoxTile'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CheckBoxTilewidget()));
            },
          ),
          ListTile(
            title: Text('DropDown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropDownwidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Radiowiget()));
            },
          )
        ],
      ),
    );
  }
}
